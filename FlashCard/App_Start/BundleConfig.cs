﻿using System.Web;
using System.Web.Optimization;

namespace FlashCard
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Script/Main").Include(
                        "~/Scripts/jquery-2.1.4.js",
                        "~/Scripts/bootstrap.js"

                        ));
            bundles.Add(new ScriptBundle("~/Script/Admin").Include(
                      "~/Scripts/moment.js",
                      "~/Scripts/npm.js",
                      "~/Scripts/app.js",
                      "~/Scripts/Base64Helper.js"
                      ));

            bundles.Add(new ScriptBundle("~/Script/Frontend").Include(
                     "~/Scripts/scripts.js",
                     "~/Scripts/Base64Helper.js"
                     ));
            bundles.Add(new ScriptBundle("~/Script/select2").Include(
                    "~/Scripts/select2/js/select2.full.js"
                    ));
            bundles.Add(new ScriptBundle("~/Script/datatables").Include(
                    "~/Scripts/datatables/js/jquery.dataTables.js",
                    "~/Scripts/datatables/js/dataTables.tableTools.js",
                    "~/Scripts/datatables/js/dataTables.colReorder.js",
                    "~/Scripts/datatables/js/dataTables.scroller.js",
                    "~/Scripts/datatables/js/dataTables.bootstrap.js"

                    ));
            bundles.Add(new StyleBundle("~/Css/Main").Include(
                        "~/Content/css/shared/jquery.ui.core.css",
                        "~/Content/css/shared/jquery.ui.resizable.css",
                        "~/Content/css/shared/jquery.ui.selectable.css",
                        "~/Content/css/shared/jquery.ui.accordion.css",
                        "~/Content/css/shared/jquery.ui.autocomplete.css",
                        "~/Content/css/shared/jquery.ui.button.css",
                        "~/Content/css/shared/jquery.ui.dialog.css",
                        "~/Content/css/shared/jquery.ui.slider.css",
                        "~/Content/css/shared/jquery.ui.tabs.css",
                        "~/Content/css/shared/jquery.ui.datepicker.css",
                        "~/Content/css/shared/jquery.ui.progressbar.css",
                        "~/Content/css/shared/bootstrap.css",
                        "~/Content/css/shared/bootstrap-theme.css",
                        "~/Content/css/shared/bootstrap-flipped.css",
                        "~/Content/css/shared/bootstrap-rtl.css",
                        "~/Content/css/shared/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Css/Admin").Include(
                       "~/Content/css/admin/essentials.css",
                       "~/Content/css/admin/layout.css",
                       "~/Content/css/admin/layout-RTL.css",
                       "~/Content/css/admin/layout-datatables.css",
                       "~/Content/css/admin/darkblue.css"));

            bundles.Add(new StyleBundle("~/Css/Frontend").Include(
                      "~/Content/css/frontend/essentials.css",
                      "~/Content/css/frontend/layout.css",
                      "~/Content/css/frontend/layout-RTL.css",
                      "~/Content/css/frontend/styleswitcher.css",
                      "~/Content/css/frontend/header-1.css",
                       "~/Content/css/frontend/red.css",
                      "~/Content/css/frontend/layout-datatables.css"
                     ));

            bundles.Add(new StyleBundle("~/Css/datatebles").Include(
                      "~/Content/css/shared/datatebles/jquery.dataTables.css",

                      "~/Content/css/shared/datatebles/jquery.dataTables_themeroller.css"
                     ));
        }
    }
}